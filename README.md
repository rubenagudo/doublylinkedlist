# DoublyLinkedList
## Asunciones

He asumido que por "enfoque académico" os referíais a que he de implementar la lista por mi mismo, asi como sus métodos.

## Implementación

Para implementar la lista he creado la clase, que implementa la interfaz List, para convertirla en una lista en pleno derecho dentro de Java. La implementación es genérica, y utiliza el método `equals` del tipo utilizado, por lo que es responsabilidad de la clase que se va a guardar proporcionar los métodos de comparación.

He implementado todos los métodos menos `retainAll` que es opcional.

Algunos de los métodos han sido implementados de una manera muy prágmatica, utilizando las implementaciones de otras clases, por ejemplo, `iterator` y `listIterator`, que son accesibles una vez conviertes la clase a un `List`. No es la implementación mas eficiente, pero lo consigo en O(2N).

El resto de funcionalidades principales, tienen las siguientes complejidades temporales

* `add`: O(1)
* `remove`: O(n)
* `contains` O(n)
* `set`: O(n)
* Variantes `*All`: O(n), y se han reutilizado los métodos simples, es decir, `addAll`, ha utilizado la implementación de `add` para cumplir su objetivo.

Se ha intentado seguir los principios DRY, repitiendo la menor cantidad de código posible y manteniendo los casos particulares al mínimo.


## Validaciones

Cada vez que se intenta acceder a elementos mediante `index` se comprueba que no sea mayor que la lista o menos que 0, y en ese caso se lanza una excepción `ArrayIndexOutOfBoundsException`

## Tests

Los tests se han realizado utilizando jUnit 5, y he intentado probar todas y cada una de las situaciones.

He conseguido un test coverage completo, tanto de clase, como de método, y de línea.