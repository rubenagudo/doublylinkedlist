package org.ragudosa.inditex;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;


class DoublyLinkedListTest {

    private DoublyLinkedList<Integer> list;
    private DoublyLinkedList<String> list2;

    @BeforeEach
    void setUp() {

        list = new DoublyLinkedList<>();
        list2 = new DoublyLinkedList<>();
    }

    @AfterEach
    void tearDown() {
        list = null;
        list2 = null;
    }


    @Test
    void add() throws Exception {
        list.add(1);
        assertEquals(list.getLast(), (Integer) 1);
        assertEquals(list.getFirst(), (Integer) 1);

        list.add(2);
        assertEquals(list.getFirst(), (Integer) 1);
        assertEquals(list.getLast(), (Integer) 2);
    }

    @Test
    void removeElement() throws Exception {
        addTestElements();

        list2.add("abc1");
        list2.add("abc2");
        list2.add("abc3");
        list2.add("abc4");
        list2.add("abc5");

        //Checking the size
        assertEquals(list.size(), 5);
        assertEquals(list2.size(), 5);
        Boolean result2 = list2.remove("abc1");
        Boolean result = list.remove((Integer) 2);
        assertEquals(result, true);
        assertEquals(result2, true);
        assertEquals(list.size(), 4);
        assertEquals(list2.size(), 4);
        assertEquals(false, list.contains(2));
        assertEquals(false, list2.contains("abc1"));


    }

    private void addTestElements() {
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
    }

    @Test
    void removeLastElement() throws Exception {
        addTestElements();

        //Checking the size
        assertEquals(list.size(), 5);

        //removing the last element
        Boolean result = list.remove((Integer) 5);
        assertEquals(result, true);
        assertEquals(list.size(), 4);
        assertEquals(false, list.contains(5));

    }

    @Test
    void removeFirstElement() throws Exception {
        addTestElements();

        //Checking the size
        assertEquals(list.size(), 5);

        //removing the first element
        Integer result = list.remove(0);
        assertEquals((Integer) 1, result);
        assertEquals(list.size(), 4);
        assertEquals((Integer)2, list.get(0));
    }

    @Test
    void removeNonExistentElement() throws Exception {
        addTestElements();

        //Checking the size
        assertEquals(list.size(), 5);

        //Removing an element that doesn't exist
        Boolean result = list.remove((Integer) 20);
        assertEquals(false, result);
    }

    @Test
    void addElementToTheMiddle() throws Exception {
        addTestElements();

        //adding a test element in the middle
        list.add(2, 99);
        assertEquals(list.size(), 6);

        //Then we try removing it from that position
        Integer element = list.get(2);
        assertEquals(element, (Integer) 99);
    }

    @Test
    void addElementToTheBeginning() throws Exception {
        list.add(0, 99);
        assertEquals(list.getFirst(), (Integer) 99);

        addTestElements();

        //adding a test element to the beginning of the list
        list.add(0, 100);
        assertEquals(list.size(), 7);
        //Then we try removing it from that position
        int element = list.indexOf((Integer) 100);
        assertEquals(element, 0);
    }

    @Test
    void addElementToTheEnd() throws Exception {
        addTestElements();

        //adding a test element to the end
        list.add(5, 99);
        assertEquals(list.size(), 6);

        //Then we try removing it from that position
        Integer element = list.get(5);
        assertEquals((Integer) 99, element);

    }

    @Test
    void addElementToNonExistentIndex() throws Exception {
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> {
            list.add(20, 20);
        });
    }

    @Test
    void addElementToNegativeIndex() throws Exception {
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> {
            list.add(-1, 20);
        });
    }

    @Test
    void isEmptyTrue() throws Exception {

        assertEquals(list.isEmpty(), true);

        list.add(1);
        list.remove((Integer) 1);

        assertEquals(list.isEmpty(), true);

    }

    @Test
    void isEmptyFalse() throws Exception {

        list.add(1);
        assertEquals(list.isEmpty(), false);

    }

    @Test
    void size() throws Exception {
        assertEquals(0, list.size());
        list.add(1);
        assertEquals(1, list.size());
    }


    @Test
    void removeIndex() throws Exception {
        addTestElements();

        Integer result = list.remove(4);
        assertEquals(result, (Integer) 5);
    }

    @Test
    void removeIndexMiddle() throws Exception {
        addTestElements();

        Integer result = list.remove(2);
        assertEquals(result, (Integer) 3);
    }

    @Test
    void removeIndexNegative() throws Exception {
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> {
            list.remove(-1);
        });
    }

    @Test
    void addAllNormal() throws Exception {
        List<Integer> elementsToAdd = Arrays.asList(1, 2, 3, 4, 5, 6);

        Boolean result = list.addAll(elementsToAdd);
        assertEquals(true, result);
        assertEquals(6, list.size());
    }

    @Test
    void addAllEmptyList() throws Exception {
        List<Integer> elementsToAdd = new ArrayList<>();

        Boolean result = list.addAll(elementsToAdd);
        assertEquals(false, result);
        assertEquals(list.size(), 0);
    }

    @Test
    void addAllInIndexNormal() throws Exception {
        addTestElements();

        List<Integer> elementsToAdd = Arrays.asList(99,999,9999,99999,9,91);
        int addedToIndex = 2;
        Boolean result = insertAndAssertAddToAll(elementsToAdd, addedToIndex);
        assertEquals(true, result);
        assertEquals(11, list.size());
    }

    @Test
    void addAllToEmptyListIndex() throws Exception {
        List<Integer> elementsToAdd = Arrays.asList(99,99,99,99,99,99);

        int addedToIndex = 0;
        Boolean result = insertAndAssertAddToAll(elementsToAdd, addedToIndex);

        assertEquals(true, result);
        assertEquals(6, list.size());
    }

    @Test
    void addAllInIndexBeginning() throws Exception {
        addTestElements();

        List<Integer> elementsToAdd = Arrays.asList(99,99,99,99,99,99);

        int addedToIndex = 0;
        Boolean result = insertAndAssertAddToAll(elementsToAdd, addedToIndex);

        assertEquals(true, result);
        assertEquals(11, list.size());
    }

    private Boolean insertAndAssertAddToAll(List<Integer> elementsToAdd, int addedToIndex) {
        Boolean result = list.addAll(addedToIndex, elementsToAdd);
        for(int i = 0; i < elementsToAdd.size(); i++) {
            assertEquals(elementsToAdd.get(i), list.get(i+addedToIndex));
        }
        return result;
    }

    @Test
    void addAllInIndexEnd() throws Exception {
        addTestElements();
        List<Integer> elementsToAdd = Arrays.asList(99,99,99,99,99,99);

        int addedToIndex = list.size();
        Boolean result = insertAndAssertAddToAll(elementsToAdd, addedToIndex);
        assertEquals(true, result);
        assertEquals(11, list.size());
    }

    @Test
    void addAllIndexNegative() throws Exception {
        List<Integer> elementsToAdd = Arrays.asList(99,99,99,99,99,99);
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> {
            list.addAll(-1, elementsToAdd);
        });
    }

    @Test
    void set() throws Exception {
        addTestElements();

        Integer element = list.set(1, 10);
        assertEquals((Integer) 2, element);
        assertEquals((Integer) 10, list.get(1));
    }

    @Test
    void setNegativeIndex() throws Exception {
        addTestElements();
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> {
            list.set(-1, 10);
        });
    }

    @Test
    void toArray() throws Exception {
        List<Integer> elementsToAdd = Arrays.asList(1, 2, 3, 4, 5, 6);
        list.addAll(elementsToAdd);
        Object[] array = list.toArray();

        assertArrayEquals(elementsToAdd.toArray(), array);
    }

    @Test
    void containsTrue() throws Exception {
        addTestElements();
        Boolean result = list.contains(5);
        assertEquals(true, result);
    }

    @Test
    void containsFalseNull() throws Exception {
        addTestElements();
        Boolean result = list.contains(null);
        assertEquals(false, result);
    }

    @Test
    void containsFalse() throws Exception {
        addTestElements();
        Boolean result = list.contains(100);
        assertEquals(false, result);
    }

    @Test
    void containsAllFalse() throws Exception {
        addTestElements();
        Boolean result = list.containsAll(Arrays.asList(99,27, 2, 3));
        assertEquals(false, result);
    }

    @Test
    void containsAllTrue() throws Exception {
        addTestElements();
        Boolean result = list.removeAll(Arrays.asList(1,2,3));
        assertEquals(true, result);
    }

    @Test
    void clear() throws Exception {
        addTestElements();
        list.clear();

        assertEquals(0, list.size());
        assertEquals(null, list.getFirst());
        assertEquals(null, list.getLast());
    }

    @Test
    void subListOneElement() throws Exception {
        addTestElements();
        Object[] subList = list.subList(1, 2).toArray();

        assertArrayEquals(Arrays.asList(2).toArray(), subList);
    }

    @Test
    void subListMultipleElements() throws Exception {
        addTestElements();
        Object[] subList = list.subList(1, 4).toArray();

        assertArrayEquals(Arrays.asList(2,3,4).toArray(), subList);
    }

    @Test
    void subListNoElements() throws Exception {
        List<Integer> elementsToAdd = Arrays.asList(1, 2, 3, 4, 5, 6);
        list.addAll(elementsToAdd);
        Object[] subList = list.subList(4, 1).toArray();

        assertArrayEquals(Arrays.asList().toArray(), subList);
    }

    @Test
    void indexOfExistingElement() throws Exception {
        addTestElements();
        Integer indexOf3 = list.indexOf(3);

        assertEquals((Integer) 2, indexOf3);
    }

    @Test
    void indexOfNonExistingElement() throws Exception {
        addTestElements();
        Integer indexOf3 = list.indexOf(99);

        assertEquals((Integer)(-1), indexOf3);
    }

    @Test
    void removeAllExisting() throws Exception {
        addTestElements();
        Boolean result = list.removeAll(Arrays.asList(1,2,3));

        assertEquals(true, result);
        assertEquals(2, list.size());
    }

    @Test
    void removeExistingAndNonExistingElements() throws Exception {
        addTestElements();
        Boolean result = list.removeAll(Arrays.asList(1,2,3,99,27));

        assertEquals(true, result);
        assertEquals(2, list.size());
    }

    @Test
    void removeNonExistingElements() throws Exception {
        addTestElements();
        Integer[] listBefore = list.toArray(new Integer[0]);

        Boolean result = list.removeAll(Arrays.asList(99,27));

        assertEquals(false, result);
        assertEquals(5, list.size());
        assertArrayEquals(listBefore, list.toArray(new Integer[0]));
    }

    @Test
    void lastIndexOfLastPosition() throws Exception {
        List<Integer> elementsToAdd = Arrays.asList(6, 6, 6, 6, 6, 6);
        list.addAll(elementsToAdd);
        Integer result = list.lastIndexOf(6);

        assertEquals((Integer) 5, result);
    }

    @Test
    void lastIndexOfMiddlePosition() throws Exception {
        List<Integer> elementsToAdd = Arrays.asList(3, 3, 3, 4, 5, 6);
        list.addAll(elementsToAdd);
        Integer result = list.lastIndexOf(3);

        assertEquals((Integer) 2, result);
    }

    @Test
    void lastIndexOfFirstPosition() throws Exception {
        List<Integer> elementsToAdd = Arrays.asList(1, 2, 3, 4, 5, 6);
        list.addAll(elementsToAdd);
        Integer result = list.lastIndexOf(1);

        assertEquals((Integer) 0, result);
    }

    @Test
    void lastIndexOfNotFound() throws Exception {
        List<Integer> elementsToAdd = Arrays.asList(1, 2, 3, 4, 5, 6);
        list.addAll(elementsToAdd);
        Integer result = list.lastIndexOf(10);

        assertEquals((Integer) (-1), result);
    }

    @Test
    void toArrayOfTypeWithEmptyArrayDestination() throws Exception {
        List<Integer> elementsToAdd = Arrays.asList(1, 2, 3, 4, 5, 6);
        list.addAll(elementsToAdd);
        Integer[] array = list.toArray(new Integer[0]);

        assertArrayEquals(elementsToAdd.toArray(), array);
    }

    @Test
    void toArrayOfTypeWithArrayOfSize() throws Exception {
        List<Integer> elementsToAdd = Arrays.asList(1, 2, 3, 4, 5, 6);
        list.addAll(elementsToAdd);
        Integer[] array = list.toArray(new Integer[6]);

        assertArrayEquals(elementsToAdd.toArray(), array);
    }

    @Test
    void toArrayOfTypeWithArrayOfBiggerSize() throws Exception {
        List<Integer> elementsToAdd = Arrays.asList(1, 2, 3, 4, 5, 6);
        List<Integer> elementsOfNull = Arrays.asList(null, null, null, null);
        list.addAll(elementsToAdd);
        Integer[] array = list.toArray(new Integer[10]);

        Integer[] sliceOfNumbers = Arrays.copyOfRange(array, 0, 6);
        Integer[] sliceOfNulls = Arrays.copyOfRange(array, 6, array.length);

        assertArrayEquals(elementsToAdd.toArray(), sliceOfNumbers);
        assertArrayEquals(elementsOfNull.toArray(), sliceOfNulls);
    }

    @Test
    void iterator() throws Exception {
        addTestElements();
        Iterator<Integer> iterator = list.iterator();

        int i = 0;
        while(iterator.hasNext()) {
            Integer element = iterator.next();
            assertEquals(list.get(i++), element);
        }
    }

    @Test
    void listIteratorIndex() throws Exception {
        addTestElements();
        int index = 3;
        ListIterator<Integer> iterator = list.listIterator(index);

        while(iterator.hasNext()) {
            Integer element = iterator.next();
            assertEquals(list.get(index++), element);
        }
    }

    @Test
    void listIterato() throws Exception {
        addTestElements();
        ListIterator<Integer> iterator = list.listIterator();

        int i = 0;
        while(iterator.hasNext()) {
            Integer element = iterator.next();
            assertEquals(list.get(i++), element);
        }
    }

    @Test
    void retainAll() throws Exception {
        assertThrows(UnsupportedOperationException.class, () -> {
            List<Integer> elementsToAdd = Arrays.asList(1, 2, 3, 4, 5, 6);
            list.retainAll(elementsToAdd);
        });
    }

}