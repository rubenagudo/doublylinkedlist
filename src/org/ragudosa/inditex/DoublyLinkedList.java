package org.ragudosa.inditex;

import java.util.*;
import java.util.function.Consumer;

/**
 * DoubleLinkedList<E>
 */
public class DoublyLinkedList<E> implements List<E> {

    /**
     * Private inner class
     *
     * @param <Type>
     */
    class Node<Type> {

        Type value;
        Node<Type> next;
        Node<Type> previous;

         Node() {}
    }

    private int size;
    private Node<E> first = null;
    private Node<E> last = null;

    DoublyLinkedList() {
        size = 0;
    }

    //public API

    /**
     * Adds the element to the end of the list
     * Time complexity: O(1)
     */
    @Override
    public boolean add(E element) {
        Node<E> newNode = new Node<>();
        newNode.value = element;

        if (isEmpty()) {
            first = newNode;
            last = newNode;
        } else {
            Node<E> temp = last;
            last = newNode;

            last.previous = temp;
            temp.next = last;
        }

        size++;

        return true;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        //Contains all is an intersection of the 2 lists, so it doesn't matter which way we compare
        return c.containsAll(Arrays.asList(toArray()));
    }

    @Override //Time complexity O(n)
    public boolean addAll(Collection<? extends E> c) {
        Consumer<E> consumer = this::add;
        int before = size;
        c.forEach(consumer);
        int after = size;
        return after != before;
    }

    @Override //Time complexity O(n)
    public boolean addAll(int index, Collection<? extends E> c) {
        int before = size;
        for (E element : c) {
            add(index++, element);
        }
        int after = size;
        return after != before;
    }

    @Override //Time complexity O(n)
    public boolean removeAll(Collection<?> c) {
        Consumer<Object> consumer = this::remove;
        int before = size;
        c.forEach(consumer);
        int after = size;
        return after != before;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override //Time complexity O(1)
    public void clear() {
        this.first = null;
        this.last = null;
        this.size = 0;
    }

    @Override //Time complexity O(n)
    public E get(int index) {
        validateIndex(index);
        Node<E> temp = navigate(index);
        return temp.value;
    }

    private Node<E> navigate(int index) {
        Node<E> current = first;
        for (int i = 0; i < index; i++) {
            current = current.next;
        }
        return current;
    }

    @Override //Time complexity O(n)
    public E set(int index, E element) {

        validateIndex(index);

        Node<E> current = navigate(index);
        E valueToReturn = current.value;
        current.value = element;
        return valueToReturn;
    }

    private void validateIndex(int index) {
        if (index > size) {
            throw new ArrayIndexOutOfBoundsException("Can't add to an index bigger than the list'");
        }

        if (index < 0) {
            throw new ArrayIndexOutOfBoundsException("Index can't be negative");
        }
    }

    /**
     * Adds the element to the i index of the list
     * Time complexity: O(n)
     */
    @Override
    public void add(int index, E element) {
        Node<E> newNode = new Node<>();
        newNode.value = element;

        validateIndex(index);

        if (isEmpty()) {
            add(element);
            return;
        }

        int i = 0;
        Node<E> current = first;
        Node<E> previous = null;

        while (i++ < index) {
            previous = current;
            current = current.next;
        }

        //this will always happen
        newNode.previous = previous;
        newNode.next = current;

        if (previous == null) { //first element
            current.previous = newNode;
            first = newNode;

        } else if (current == null) {
            previous.next = newNode;
            last = newNode;

        } else {
            previous.next = newNode;
            current.previous = newNode;
        }

        size++;
    }

    @Override //Time complexity O(n)
    public E remove(int index) {
        Node<E> temp = first;

        validateIndex(index);

        if (index == 0) {
            E valueToReturn = first.value;
            first = first.next;
            size--;
            return valueToReturn;

        }

        if (index == size - 1) {
            E valueToReturn = last.value;
            last = last.previous;
            size--;
            return valueToReturn;
        }

        int i = 0;

        while (i < index) {
            temp = temp.next;
            i++;
        }

        temp.previous = temp.next;
        temp.next = temp.previous;
        size--;
        return temp.value;
    }

    @Override //Time complexity O(n)
    public int indexOf(Object o) {
        Node<E> current = first;
        int index = 0;
        while (current != null) {
            if ((isEquals(o, current))) return index;
            index++;
            current = current.next;
        }
        return -1;
    }

    //this cries for some refactoring
    @Override //Time complexity O(n)
    public int lastIndexOf(Object o) {
        Node<E> current = last; //Using last will provide an average best result for this method, first hit will be the result
        int index = size - 1;

        while (current != null) {
            if ((isEquals(o, current))) return index;
            index--;
            current = current.previous;
        }
        return -1;

    }

    @Override
    public ListIterator<E> listIterator() {
        return (ListIterator<E>) Arrays.asList(toArray(new Object[0])).listIterator();
    }

    @Override
    public ListIterator<E> listIterator(int index) {

        return (ListIterator<E>) Arrays.asList(toArray(new Object[0])).listIterator(index);
    }

    @Override //Time complexity O(n)
    public List<E> subList(int fromIndex, int toIndex) {
        List<E> subList = new LinkedList<>();
        Node<E> current = navigate(fromIndex);

        validateIndex(fromIndex);
        validateIndex(toIndex);

        for (int i = fromIndex; i < toIndex; i++) {
            subList.add(current.value);
            current = current.next;
        }

        return subList;
    }

    @Override //Time complexity O(n)
    public boolean remove(Object element) {
        int i = 0;
        Node<E> currentNode = this.first;

        while (i < size) {
            if (isEquals(element, currentNode)) {

                if (size == 1) { //if the list only has one element we set it to null
                    this.first = null;
                    this.last = null;
                } else if (i == 0) { //first element when there are multiple elements
                    this.first = currentNode.next;
                    this.first.previous = null;
                } else if (i == size - 1) { //last element when there are multiple elements
                    this.last = currentNode.previous;
                    this.last.next = null;
                } else { //general case
                    currentNode.previous.next = currentNode.next;
                    currentNode.next.previous = currentNode.previous;
                }

                size--;
                return true;

            } else {
                i++;
                currentNode = currentNode.next;
            }
        }

        return false;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    @Override //Time complexity O(n)
    public boolean contains(Object o) {
        Node<E> current = first;
        while (current != null) {
            Boolean isEqual = isEquals(o, current);
            if (isEqual) return true;

            current = current.next;
        }

        return false;
    }

    private boolean isEquals(Object o, Node<E> current) {
        return o == null ? current.value == null : o.equals(current.value);
    }

    @Override
    public Iterator<E> iterator() {
        return (Iterator<E>) Arrays.asList(toArray(new Object[0])).iterator();
    }

    @Override //Time complexity O(n)
    public Object[] toArray() {
        return toArray(new Object[size]);
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        if (a.length < size)
            a = (T1[]) java.lang.reflect.Array.newInstance(
                    a.getClass().getComponentType(), size);
        int i = 0;
        Object[] result = a;
        for (Node<E> x = first; x != null; x = x.next)
            result[i++] = (T1) x.value;

        if (a.length > size)
            a[size] = null;

        return a;
    }

    public int size() {
        return size;
    }

    E getLast() {
        if (last == null) return null;
        return last.value;
    }

    E getFirst() {
        if (first == null) return null;
        return first.value;
    }
}